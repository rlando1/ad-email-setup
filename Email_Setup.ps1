﻿# By: Landon Lengyel
# Last Updated: May 14th, 2019
#
#


# list all domains you wish to enable when using the script. First being the primary domain of the account (SMTP). All others being secondary domains (smtp)
$primaryDomain = "example.com"
$secondaryDomains = @("eff.org", "opensuse.org")

# change search base
# this value requires quotes around it
$searchBase = "OU=Texas,DC=texas,DC=example,DC=com"



#
#
#
# --- Don't edit anything below here unless you know what you're doing ---
#
#
#



# prompt user to confirm that the account is the correct account
# returns true if user confirmed account - false if user did not
function Confirm-Account ($ADAccount) {
    Write-Host
    Write-Host
    Write-Host "samAccountName: "$ADAccount.SamAccountName
    Write-Host "User Pricipal Name (UserPrincipalName): "$ADAccount.UserPrincipalName
    Write-Host "Distinguished Name: "$ADAccount.DistinguishedName
    Write-Host "objectGUID: "$ADAccount.objectGUID
    Write-Host

    $confirmation = Read-Host "Please confirm that the following information is accurate to the AD account you want to edit (Y/N)"
    if ($confirmation -eq "y" -or $confirmation -eq "yes"){ 
        clear
        # return true for confirmation
        return $true
    }
    else {
        Write-Host "Account confirmation failed" -ForegroundColor Red
        # return false for failure
        return $false
    }
}

# modify the proxyAddress attribute in a given AD account
# returns true if it successfully modified the attributes - returns false if it failed
function Modify-Proxy-Addresses ($ADAccount) {
    $primaryEmail = $null

    # verify the account does not already have proxy addresses
    # request everything before the '@'
    if (-Not $ADAccount.proxyAddresses) {

        do {
            clear

            # Write-Host formatting
            Write-Host "Please enter the `"local part`" of the email you want to use. " -NoNewline; Write-Host "richard.stallman" -ForegroundColor Green -NoNewline; Write-Host "@example.com"
            $localPart = Read-Host "Local Part"

            if ([string]::IsNullOrWhiteSpace($localPart) ){
                Write-Host "You must input the local part of the account email" -ForegroundColor Red
                # press just about any key to continue
                [void](Read-Host "Press Enter to continue")
            }
        } while ( [string]::IsNullOrWhiteSpace($localPart) )
    }

    # room to expand here
    else {
        Write-Error "Account currently has proxyAddresses assigned to them. At the moment, this script only supports adding proxyAddresses, not modifying them."
        exit
    }

    # prompt user to confirm address(es)
    clear
    Write-Host "Please confirm the following addresses"
    Write-Host
    Write-Host "SMTP:$($localPart + '@' + $primaryDomain)"
    foreach ($domain in $secondaryDomains) {
        Write-Host "smtp:$($localPart + '@' + $domain)"
    }
    Write-Host
    $confirmation = Read-Host "User confirmation (Y/N)"
    
    if ($confirmation -eq "y" -or $confirmation -eq "yes") { 
        # just continue
    }
    else {
        Write-Host "User confirmation failed" -ForegroundColor Red
        # return false for failure
        return $false
    }

    # attempt to change proxy addresses
    try {
        clear

        # handle primary SMTP
        Write-Host "Adding primary address SMTP:$($localPart + '@' + $primaryDomain)"
        $ADAccount.proxyAddresses.add( "SMTP:$($localPart + '@' + $primaryDomain)" )

        # handle secondary smtp
        foreach ($domain in $secondaryDomains) {
            Write-Host "Adding secondary address smtp:$($localPart + '@' + $domain)"
            $ADAccount.proxyAddresses.add( "smtp:$($localPart + '@' + $domain)" )
        }

        Write-Host
        Write-Host "Pushing changes to AD..."
        $result = Set-ADUser -Instance $ADAccount

        # add white space
        Write-Host
        Write-Host

        # return true for success
        return $true
    }
    catch {
        Write-Error "Failed to set account proxy addresses - script will now exit"
        # return false for failure
        return $false
    }
}

function Modify-UPN($ADAccount){
    # verify change information with colored text for visibility
    Write-Host "Confirm change from "$ADAccount.UserPrincipalName -ForegroundColor Yellow -NoNewline; Write-Host " -> " -NoNewline; Write-Host ($ADAccount.SamAccountName + '@' + $primaryDomain) -ForegroundColor Green

    $confirmation = Read-Host "Please confirm that the following information is accurate to the AD account you want to edit (Y/N)"
    if ($confirmation -eq "y" -or $confirmation -eq "yes"){ 
        clear
    }
    else {
        Write-Host "Confirmation failed. Script will now exit" -ForegroundColor Red
        exit
    }


    # attempt to change the user's domain
    try {
        Write-Host "Attempting to change domain..."
        $newUserPrincipalName = $ADAccount.sAMAccountName + "@" + $primaryDomain
        #$ADAccount.SamAccountName = $newUserPrincipalName
        $ADAccount.UserPrincipalName = $newUserPrincipalName

        Write-Host
        Write-Host "Pushing changes to AD..."
        $result = Set-ADUser -Instance $ADAccount

        clear

        # return true for success
        return $true
    }
    catch {
        Write-Error "Failed to change user domain"
        # return false for failure
        return $false
    }
}



############### "Main" ###############

clear

Import-Module activeDirectory


do {
    $username = Read-Host "Username"
    

    # display only if user did not input the name
    if ([string]::IsNullOrWhiteSpace($username) ){
        Write-Host "You must enter a username to continue." -ForegroundColor Red
        pause # wait for keypress so user can read error message
    }
} while ( [string]::IsNullOrWhiteSpace($username) )
clear

Write-Host "Gathering data. Please wait..."
try { $ADAccount = Get-ADUser -Filter "SAMAccountName -like '$username*'" }
catch {
    Write-Error "An error gathering account data has occurred. The script will now exit"
    exit
}

# ensure found AD account is the correct account
# $success -eq $true if account confirmed
$confirmSuccess = Confirm-Account($ADAccount)

if ($confirmSuccess -eq $true) {
    # begin modification of AD proxy addresses
    # $proxySuccess -eq $true if proxy address(es) were added
    $proxySuccess = Modify-Proxy-Addresses($ADAccount)
}
if ($proxySuccess -eq $True){
    # begin modification of AD user domain
    # $domainChangeSuccess -eq $true if domain was successfully changed to the primary domain
    $domainChangeSuccess = Modify-UPN($ADAccount)
}

# tell user to verify in AD and to enable the account on the provider side
if ($proxySuccess -eq $true -and $domainChangeSuccess -eq $true) {
    Write-Host "Script was reported successful"
    Write-Host "To be certain, please manually verify the user's AD Account for the proxy addresses (proxyAddresses)"
    Write-Host "Remember, the emails will " -NoNewline; Write-Host "still need to be configured by an admin of the email provider " -ForegroundColor Red -NoNewline; Write-Host "(For example: Microsoft Office)"
    Write-Host
    Write-Host
    Write-Host
}
